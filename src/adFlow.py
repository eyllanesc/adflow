import sys

from PyQt5.QtCore import QFile, QTextStream
from PyQt5.QtGui import QIcon, QPixmap
from PyQt5.QtWidgets import QMainWindow, QApplication, QMenuBar, QMenu, qApp, QFrame, QWidget, QVBoxLayout, QHBoxLayout, \
    QLabel, QStackedLayout, QPushButton


class TopFrame(QFrame):
    def __init__(self, parent=None):
        QFrame.__init__(self, parent=parent)
        self.setLayout(QHBoxLayout())

        label1 = QLabel("""<html><head/><body><p>
        <span style=" color:#ab9ba9;">Tenger AdFlow</span>
        </p></body></html>""", self)
        self.layout().addWidget(label1)

        label2 = QLabel("""<html><head/><body><p>
        <span style=" font-style:italic;">Beta Version 0.1.0 Limited Release</span>
        </p></body></html>""", self)
        self.layout().addWidget(label2)

        label3 = QLabel("""Smaller - Faster - Cheaper""", self)
        self.layout().addWidget(label3)

        frame = QFrame(self)
        frame.setFrameShape(QFrame.StyledPanel)
        frame.setFrameShadow(QFrame.Raised)
        frame.setObjectName("Logo_frame")
        self.layout().addWidget(frame)

        self.setFixedHeight(80)
        self.raise_()


class FootFrame(QFrame):
    def __init__(self, parent=None):
        QFrame.__init__(self, parent=parent)
        self.setLayout(QVBoxLayout())
        self.layout().addWidget(QLabel("""<html><head/><body><p align="center">
        Copyright 2017 Tengerdata Technologies Ltd. www.tengerdata.com
        </p></body></html>""", self))

        self.setFixedHeight(80)
        self.setMinimumWidth(800)
        self.raise_()


class MainFrame(QFrame):
    def __init__(self, parent=None):
        QFrame.__init__(self, parent=parent)
        self.setObjectName("MainFrame")
        self.raise_()
        self.setLayout(QStackedLayout(self))

        btn1 = QPushButton("btn1")
        self.layout().addWidget(btn1)

        btn2 = QPushButton("btn2")
        self.layout().addWidget(btn2)
        btn1.clicked.connect(lambda: self.layout().setCurrentIndex(1))
        btn2.clicked.connect(lambda: self.layout().setCurrentIndex(0))


class AdFlow(QMainWindow):
    def __init__(self, parent=None):
        QMainWindow.__init__(self, parent=parent)
        self.setWindowTitle("Tengerdata - AdFlow")

        icon = QIcon()
        icon.addPixmap(QPixmap(":/images/favicon"), QIcon.Normal, QIcon.Off)

        self.setWindowIcon(icon)

        widget = QWidget(self)
        self.setCentralWidget(widget)
        widget.setLayout(QVBoxLayout())
        widget.layout().addWidget(TopFrame())
        widget.layout().addWidget(MainFrame())
        widget.layout().addWidget(FootFrame())

        self.create_menu_bar()

    def create_menu_bar(self):
        menubar = QMenuBar(self)
        self.setMenuBar(menubar)

        menuFile = QMenu(menubar)
        menuFile.setTitle("File")
        menubar.addAction(menuFile.menuAction())

        menuHelp = QMenu(menubar)
        menuHelp.setTitle("Help")
        menubar.addAction(menuHelp.menuAction())

        menuExit = QMenu(menubar)
        menuExit.setTitle("Exit")
        menubar.addAction(menuExit.menuAction())


import resource_rc

if __name__ == '__main__':
    app = QApplication(sys.argv)
    app.setStyle("fusion")
    w = AdFlow()

    f = QFile(":/style/stylesheets/Tengerdata.qss")
    if f.exists():
        f.open(QFile.ReadOnly | QFile.Text)
        ts = QTextStream(f)
        qApp.setStyleSheet(ts.readAll())

    w.show()
    sys.exit(app.exec_())
